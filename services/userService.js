const { UserRepository } = require('../repositories/userRepository');

class UserService {

    checkUserEmail(email) {
        if (this.search({ email: email })) {
            return true;
        }
        return false;
    }
    checkUserPhoneNum(phoneNumber) {
        if (this.search({ phoneNumber: phoneNumber })) {
            return true;
        }
        return false;
    }


    getAllUsers() {
        const items = UserRepository.getAll();
        return items;
    }

    search(search) {
        const item = UserRepository.getOne(search);
        return item;
    }

    createUser(data) {
        if (this.checkUserEmail(data.email)) {
            throw Error("User with such email is already exists");
        }
        if (this.checkUserPhoneNum(data.phoneNumber)) {
            throw Error("User with such phoneNumber is already exists");
        }
        const item = UserRepository.create(data);
        if (!item) {
            return null;
        }
        return item;
    }

    updateUser(id, data) {
        if (!this.search({ id: id })) {
            return null;
        }
        if (data.email) {
            if (this.checkUserEmail(data.email)) {
                throw Error("User with such email is already exists");
            }
        }
        if (data.phoneNumber) {
            if (this.checkUserPhoneNum(data.phoneNumber)) {
                throw Error("User with such phoneNumber is already exists");
            }
        }
        const item = UserRepository.update(id, data);
        return item;
    }
    deleteUser(id) {
        if (!this.search({ id: id })) {
            return null;
        }
        const item = UserRepository.delete(id);
        return item;
    }

}

module.exports = new UserService();