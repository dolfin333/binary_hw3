const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {

    checkFighterName(name) {
        if (this.searchFighter({ name: name })) {
            return true;
        }
        return false;
    }

    getAllFighters() {
        const items = FighterRepository.getAll();
        return items;
    }

    searchFighter(search) {
        const item = FighterRepository.getOne(search);
        return item;
    }

    createFighter(data) {
        if (this.checkFighterName(data.name)) {
            throw Error("Fighter with such name is already exists")
        }
        if (!data.health) {
            data.health = 100;
        }
        const item = FighterRepository.create(data);
        if (!item) {
            return null;
        }
        return item;
    }

    updateFighter(id, data) {
        if (!this.searchFighter({ id: id })) {
            return null;
        }
        if (data.name) {
            if (this.checkFighterName(data.name)) {
                throw Error("Fighter with such name is already exists")
            }
        }
        const item = FighterRepository.update(id, data);
        return item;
    }

    deleteFighter(id) {
        if (!this.searchFighter({ id: id })) {
            return null;
        }
        const item = FighterRepository.delete(id);
        return item;
    }
}

module.exports = new FighterService();