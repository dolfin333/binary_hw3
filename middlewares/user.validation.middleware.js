const { user } = require('../models/user');

function UserValid(data) {
    let result = { status: 200, mess: "Everything ok" };
    for (const key in data) {
        if (key === "id") {
            result = { status: 400, mess: "Field id cannot be in body of request" };
            break;
        }
        if (!user.hasOwnProperty(key)) {
            result = { status: 400, mess: "User entity has isn't valid field" };
            break;
        }
        if (data[key].toString().length === 0) {
            result = { status: 400, mess: "User`s fields cannot be empty" };
            break;
        }
        if (key === "email") {
            if (data[key].split("@gmail.com").length - 1 !== 1 || data[key].split("@").length - 1 !== 1) {
                result = { status: 400, mess: "User email isn't valid" };
                break;
            }
        }
        if (key === "phoneNumber") {
            if (data[key].length !== 13 || data[key].substr(0, 4) !== "+380") {
                result = { status: 400, mess: "User phone number isn't valid" };
                break;
            }
        }
        if (key === "password") {
            if (typeof data[key] !== 'string') {
                result = { status: 400, mess: "User password should be a string" };
                break;
            }
            if (data[key].length < 3) {
                result = { status: 400, mess: "User password isn't valid" };
                break;
            }
        }
    };

    return result;
}

const createUserValid = (req, res, next) => {

    const data = req.body;

    if (Object.keys(data).length !== Object.keys(user).length - 1) {
        res.err = { status: 400, mess: "Amount of user fields isn't valid" };
        next();
    }

    const result = UserValid(data);
    if (result.status !== 200) {
        res.err = result;
        next();
    }
    next();
}

const updateUserValid = (req, res, next) => {
    const data = req.body;
    if (Object.keys(data).length === 0) {
        res.err = { status: 400, mess: "At least one user`s field should be fulled" };
        next();
    }

    const result = UserValid(data);
    if (result.status !== 200) {
        res.err = result;
        next();
    }
    next();
}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;