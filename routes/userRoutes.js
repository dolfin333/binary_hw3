const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

router.get("", function (req, res, next) {
    const users = UserService.getAllUsers();
    res.data = users;
    next();
}, responseMiddleware)

router.get("/:id", function (req, res, next) {
    const user = UserService.search({ id: req.params.id });
    if (user) {
        res.data = user;
    }
    else {
        res.err = { status: 404, mess: "User not found" };
    }
    next();
}, responseMiddleware)

router.post("", createUserValid, function (req, res, next) {
    if (!res.err) {
        try {
            const user = UserService.createUser(req.body);
            if (user) {
                res.data = user;
            }
            else {
                res.err = { status: 404, mess: "User not found" };
            }
        }
        catch (err) {
            res.err = { status: 400, mess: err.toString() }
        }
    }
    next();
}, responseMiddleware)

router.put("/:id", updateUserValid, function (req, res, next) {
    if (!res.err) {
        try {
            const user = UserService.updateUser(req.params.id, req.body);
            if (user) {
                res.data = user;
            }
            else {
                res.err = { status: 404, mess: "User not found" };
            }
        }
        catch (err) {
            res.err = { status: 400, mess: err.toString() }
        }
    }
    next();
}, responseMiddleware)

router.delete("/:id", function (req, res, next) {
    const user = UserService.deleteUser(req.params.id);
    if (user) {
        res.data = user;
    }
    else {
        res.err = { status: 404, mess: "User not found" };
    }
    next();
}, responseMiddleware)

module.exports = router;