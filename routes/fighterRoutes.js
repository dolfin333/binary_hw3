const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');

const router = Router();

// TODO: Implement route controllers for fighter
router.get("", function (req, res, next) {
    const fighters = FighterService.getAllFighters();
    res.data = fighters;
    next();
}, responseMiddleware)

router.get("/:id", function (req, res, next) {
    const fighter = FighterService.searchFighter({ id: req.params.id });
    if (fighter) {
        res.data = fighter;
    }
    else {
        res.err = { status: 404, mess: "Fighter not found" };
    }
    next();
}, responseMiddleware)

router.post("", createFighterValid, function (req, res, next) {
    if (!res.err) {
        try {
            const fighter = FighterService.createFighter(req.body);
            if (fighter) {
                res.data = fighter;
            }
            else {
                res.err = { status: 404, mess: "Fighter not found" };
            }
        }
        catch (err) {
            res.err = { status: 400, mess: err.toString() }
        }
    }
    next();
}, responseMiddleware)

router.put("/:id", updateFighterValid, function (req, res, next) {
    if (!res.err) {
        try {
            const fighter = FighterService.updateFighter(req.params.id, req.body);
            if (fighter) {
                res.data = fighter;
            }
            else {
                res.err = { status: 404, mess: "Fighter not found" };
            }
        }
        catch (err) {
            res.err = { status: 400, mess: err.toString() }
        }
    }
    next();
}, responseMiddleware)

router.delete("/:id", function (req, res, next) {
    const fighter = FighterService.deleteFighter(req.params.id);
    if (fighter) {
        res.data = fighter;
    }
    else {
        res.err = { status: 404, mess: "Fighter not found" };
    }
    next();
}, responseMiddleware)

module.exports = router;